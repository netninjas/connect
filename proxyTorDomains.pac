
function FindProxyForURL(url, host) {
	if(host.endsWith(".onion")) {
		return "PROXY netninja.oodi.co.il:9091";
	}
	return "DIRECT";
}